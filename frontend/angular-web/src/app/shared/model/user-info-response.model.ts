export class UserInfoResponseModel {

  authenticated: boolean;
  username: string;
}
