export interface Base64DecoderResultModel {
  data: string;
  value: ArrayBuffer;
}
